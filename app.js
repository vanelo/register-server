var express = require('express');
const pg = require('pg');
const path = require('path');
const app = express();
const db = require("./models/database.js");
const Router = require("express-promise-router");
var cors = require('cors');
const bodyParser = require("body-parser");
var mainRouter = new Router();

/*SERVER CONFIGURATION*/
app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});
app.use(cors());
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use("/", mainRouter);
var dbConfigured = false;

//-----------------------------------
// Routes
//-----------------------------------

/* CREATE DOCUMENT*/
mainRouter.post('/api/v1/document', async(req, res) => {
	const results = [];
	// Grab data from http request
	const data = {
		dbHost:req.body.dbHost,
		dbUser:req.body.dbUser,
		dbPass:req.body.dbPass,
		docTypecode: req.body.docTypecode,
		docNumber: req.body.docNumber,
		countryIsoCode: req.body.countryIsoCode,
		stateIsoCode: req.body.stateIsoCode,
		docImageData1: req.body.docImageData1,
		docImageData2: req.body.docImageData2,
		optionalimage1: req.body.optionalimage1,
		optionalimage2: req.body.optionalimage2
	};
	// Setup database if required
	if(!dbConfigured)
	{
		await db.setup(data.dbUser, data.dbPass, data.dbHost);
		dbConfigured = true;
	}
	const connectionString = "postgresql://" + data.dbUser + ":" + data.dbPass + "@" + data.dbHost + "/docsregister";

	pg.connect(connectionString, (err, client, done) => {
		// Handle connection errors
		if(err) {
			done();
			console.log(err);
			return res.status(500).json({success: false, data: err});
		}

		const documents = [];
		// SQL Query > Select Data: Verify if document already exist
		const querySelect = client.query('SELECT * FROM documents WHERE countryIsoCode=($1) AND stateIsoCode=($2) AND docNumber=($3) ;', 
			[data.countryIsoCode, data.stateIsoCode, data.docNumber]);
		querySelect.on('row', (row) => {
			documents.push(row);
		});
		querySelect.on('end', () => {
			done();
			if(documents.length < 1){
				// SQL Query > Insert Data
				client.query('INSERT INTO documents(docTypecode, docNumber, countryIsoCode, stateIsoCode, docImageData1, docImageData2, optionalimage1, optionalimage2) values($1, $2, $3, $4, $5, $6, $7, $8)',
				[data.docTypecode, data.docNumber, data.countryIsoCode, data.stateIsoCode, data.docImageData1, data.docImageData2, data.optionalimage1, data.optionalimage2]);
				// SQL Query > Select Data
				const query = client.query('SELECT * FROM documents ORDER BY countryIsoCode ASC');
				// Stream results back one row at a time
				query.on('row', (row) => {
					results.push(row);
				});
				// After all data is returned, close connection and return results
				query.on('end', () => {
					done();
					return res.json(results);
				});
			}else{
				return res.status(200).json({success: false, data: "DUPLICATE"});
			}
		});
	});
});

/* GET AN ESPECIFIC DOCUMENT*/
mainRouter.post('/api/v1/documentdata', async(req, res) => {
	const results = [];
	// Grab data from http request
	const data = {
		dbHost:req.body.dbHost,
		dbUser:req.body.dbUser,
		dbPass:req.body.dbPass,
		docTypecode: req.body.docTypecode,
		docNumber: req.body.docNumber,
		countryIsoCode: req.body.countryIsoCode,
		stateIsoCode: req.body.stateIsoCode
	};
	// Setup database if required
	if(!dbConfigured)
	{
		await db.setup(data.dbUser, data.dbPass, data.dbHost);
		dbConfigured = true;
	}
	const connectionString = "postgresql://" + data.dbUser + ":" + data.dbPass + "@" + data.dbHost + "/docsregister";

	// Get a Postgres client from the connection pool
	pg.connect(connectionString, (err, client, done) => {
	// Handle connection errors
		if(err) {
			done();
			console.log(err);
			return res.status(500).json({success: false, data: err});
		}

		// SQL Query > Select Data: Verify if document already exist
		const query = client.query('SELECT * FROM documents WHERE countryIsoCode=($1) AND stateIsoCode=($2) AND docTypecode=($3) AND docNumber=($4) ;', 
			[data.countryIsoCode, data.stateIsoCode,data.docTypecode , data.docNumber]);
		query.on('row', (row) => {
			results.push(row);
		});
		query.on('end', () => {
			done();
			return res.json(results);
		});
	});
});

/* GET COUNTRIES */
mainRouter.post('/api/v1/countries', async(req, res) => {
	const results = [];
	const data = {
		dbHost:req.body.dbHost,
		dbUser:req.body.dbUser,
		dbPass:req.body.dbPass
	};

	// Setup DB Config
	if(!dbConfigured)
	{
		await db.setup(data.dbUser, data.dbPass, data.dbHost);
		dbConfigured = true;
	}
    const connectionString = "postgresql://" + data.dbUser + ":" + data.dbPass + "@" + data.dbHost + "/docsregister";

	// Get a Postgres client from the connection pool
	pg.connect(connectionString, (err, client, done) => {
		// Handle connection errors
		if(err) {
			done();
			console.log(err);
			return res.status(500).json({success: false, data: err});
		}
		// SQL Query > Select Data
		const query = client.query('SELECT * FROM countries ORDER BY name ASC;');
		// Stream results back one row at a time
		query.on('row', (row) => {
		  results.push(row);
		});
		// After all data is returned, close connection and return results
		query.on('end', () => {
		  done();
		  return res.json(results);
		});
	});
});

/* GET STATES*/
mainRouter.post('/api/v1/states', async(req, res) => {
	const results = [];
	// Grab data from http request
	const data = {
		dbHost:req.body.dbHost,
		dbUser:req.body.dbUser,
		dbPass:req.body.dbPass,
		countryIsoCode: req.body.countryIsoCode
	};

	// Setup DB Config
	if(!dbConfigured)
	{
		await db.setup(data.dbUser, data.dbPass, data.dbHost);
		dbConfigured = true;
	}
	const connectionString = "postgresql://" + data.dbUser + ":" + data.dbPass + "@" + data.dbHost + "/docsregister";

	// Get a Postgres client from the connection pool
	pg.connect(connectionString, (err, client, done) => {
		// Handle connection errors
		if(err) {
			done();
			console.log(err);
			return res.status(500).json({success: false, data: err});
		}
		// SQL Query > Select Data
		const query = client.query('SELECT * FROM states WHERE countryIsoCode=($1) ORDER BY name ASC;', [data.countryIsoCode]);
		// Stream results back one row at a time
		query.on('row', (row) => {
			results.push(row);
		});
		// After all data is returned, close connection and return results
		query.on('end', () => {
			done();
			return res.json(results);
		});
	});
});

/* GET DOCTYPES */
mainRouter.post('/api/v1/doctypes', async(req, res) => {
	const results = [];
	const data = {
		dbHost:req.body.dbHost,
		dbUser:req.body.dbUser,
		dbPass:req.body.dbPass,
		countryIsoCode: req.body.countryIsoCode
	};

	// Setup DB Config
	if(!dbConfigured)
	{
		await db.setup(data.dbUser, data.dbPass, data.dbHost);
		dbConfigured = true;
	}

	const connectionString = "postgresql://" + data.dbUser + ":" + data.dbPass + "@" + data.dbHost + "/docsregister";

	// Get a Postgres client from the connection pool
	pg.connect(connectionString, (err, client, done) => {
	// Handle connection errors
	if(err) {
		done();
		console.log(err);
		return res.status(500).json({success: false, data: err});
	}
	// SQL Query > Select Data
	const query = client.query('SELECT * FROM doctypes ORDER BY name ASC;');
	// Stream results back one row at a time
	query.on('row', (row) => {
		results.push(row);
	});
	// After all data is returned, close connection and return results
	query.on('end', () => {
		done();
		return res.json(results);
	});
  });
});

/* GET DOCUMENTS */
mainRouter.post('/api/v1/documents', async(req, res) => {
	const results = [];
	const data = {
		dbHost:req.body.dbHost,
		dbUser:req.body.dbUser,
		dbPass:req.body.dbPass,
		countryIsoCode: req.body.countryIsoCode
	};

	// Setup DB Config
	if(!dbConfigured)
	{
		await db.setup(data.dbUser, data.dbPass, data.dbHost);
		dbConfigured = true;
	}
	const connectionString = "postgresql://" + data.dbUser + ":" + data.dbPass + "@" + data.dbHost + "/docsregister";

	// Get a Postgres client from the connection pool
	pg.connect(connectionString, (err, client, done) => {
	// Handle connection errors
	if(err) {
		done();
		console.log(err);
		return res.status(500).json({success: false, data: err});
	}
	// SQL Query > Select Data
	const query = client.query('SELECT docTypecode, docNumber, countryIsoCode, stateIsoCode FROM documents ORDER BY countryIsoCode ASC;');
	// Stream results back one row at a time
	query.on('row', (row) => {
		results.push(row);
	});
	// After all data is returned, close connection and return results
	query.on('end', () => {
		done();
		return res.json(results);
	});
  });
});

/* CREATE COUNTRY*/
mainRouter.post('/api/v1/country', async(req, res) => {
	const results = [];
	// Grab data from http request
	const data = {
		dbHost:req.body.dbHost,
		dbUser:req.body.dbUser,
		dbPass:req.body.dbPass,
		name: req.body.name,
		countryIsoCode:  req.body.countryIsoCode
	};
	// Setup database if required
	if(!dbConfigured)
	{
		await db.setup(data.dbUser, data.dbPass, data.dbHost);
		dbConfigured = true;
	}
	const connectionString = "postgresql://" + data.dbUser + ":" + data.dbPass + "@" + data.dbHost + "/docsregister";

	// Get a Postgres client from the connection pool
	pg.connect(connectionString, (err, client, done) => {
		// Handle connection errors
		if(err) {
			done();
			console.log(err);
			return res.status(500).json({success: false, data: err});
		}

		const countries = [];
		// SQL Query > Select Data: Verify if country already exist
		const querySelect = client.query('SELECT * FROM countries WHERE countryIsoCode=($1);', 
			[data.countryIsoCode]);
		querySelect.on('row', (row) => {
			countries.push(row);
		});
		querySelect.on('end', () => {
			done();
			if(countries.length < 1){
				// SQL Query > Insert Data
				client.query('INSERT INTO countries(name, countryIsoCode) values($1, $2)',
				[data.name, data.countryIsoCode]);
				// SQL Query > Select Data
				const query = client.query('SELECT * FROM countries ORDER BY countryIsoCode ASC');
				// Stream results back one row at a time
				query.on('row', (row) => {
				  results.push(row);
				});
				// After all data is returned, close connection and return results
				query.on('end', () => {
				  done();
				  return res.json(results);
				});
			}else{
				return res.status(200).json({success: false, data: "DUPLICATE"});
			}
		});
	});
});

/* CREATE STATE*/
mainRouter.post('/api/v1/state', async(req, res) => {
	const results = [];
	// Grab data from http request
	const data = {
		dbHost:req.body.dbHost,
		dbUser:req.body.dbUser,
		dbPass:req.body.dbPass,
		name: req.body.name,
		stateIsoCode: req.body.stateIsoCode,
		countryIsoCode:  req.body.countryIsoCode
	};
	// Setup database if required
	if(!dbConfigured)
	{
		await db.setup(data.dbUser, data.dbPass, data.dbHost);
		dbConfigured = true;
	}
	const connectionString = "postgresql://" + data.dbUser + ":" + data.dbPass + "@" + data.dbHost + "/docsregister";

	// Get a Postgres client from the connection pool
	pg.connect(connectionString, (err, client, done) => {
		// Handle connection errors
		if(err) {
			done();
			console.log(err);
			return res.status(500).json({success: false, data: err});
		}

		const states = [];
		// SQL Query > Select Data: Verify if country already exist
		const querySelect = client.query('SELECT * FROM states WHERE stateIsoCode=($1);', 
			[data.stateIsoCode]);
		querySelect.on('row', (row) => {
			states.push(row);
		});
		querySelect.on('end', () => {
			done();
			if(states.length < 1){
				// SQL Query > Insert Data
				client.query('INSERT INTO states(name, stateIsoCode, countryIsoCode) values($1, $2, $3)',
				[data.name, data.stateIsoCode, data.countryIsoCode]);
				// SQL Query > Select Data
				const query = client.query('SELECT * FROM states ORDER BY stateIsoCode ASC');
				// Stream results back one row at a time
				query.on('row', (row) => {
				  results.push(row);
				});
				// After all data is returned, close connection and return results
				query.on('end', () => {
				  done();
				  return res.json(results);
				});
			}else{
				return res.status(200).json({success: false, data: "DUPLICATE"});
			}
		});
	});
});

/* CREATE DOCUMENT TYPES*/
mainRouter.post('/api/v1/doctype', async(req, res) => {
	const results = [];
	// Grab data from http request
	const data = {
		dbHost:req.body.dbHost,
		dbUser:req.body.dbUser,
		dbPass:req.body.dbPass,
		name: req.body.name,
		docTypecode: req.body.docTypecode
	};
	// Setup database if required
	 if(!dbConfigured)
	{
		await db.setup(data.dbUser, data.dbPass, data.dbHost);
		dbConfigured = true;
	}
	const connectionString = "postgresql://" + data.dbUser + ":" + data.dbPass + "@" + data.dbHost + "/docsregister";

	// Get a Postgres client from the connection pool
	pg.connect(connectionString, (err, client, done) => {
		// Handle connection errors
		if(err) {
			done();
			console.log(err);
			return res.status(500).json({success: false, data: err});
		}

		const documentypes = [];
		// SQL Query > Select Data: Verify if country already exist
		const querySelect = client.query('SELECT * FROM doctypes WHERE docTypecode=($1);', 
			[data.docTypecode]);
		querySelect.on('row', (row) => {
			documentypes.push(row);
		});
		querySelect.on('end', () => {
			done();
			if(documentypes.length < 1){
				// SQL Query > Insert Data
				client.query('INSERT INTO doctypes(name, docTypecode) values($1, $2)',
				[data.name, data.docTypecode]);
				// SQL Query > Select Data
				const query = client.query('SELECT * FROM doctypes ORDER BY docTypecode ASC');
				// Stream results back one row at a time
				query.on('row', (row) => {
				  results.push(row);
				});
				// After all data is returned, close connection and return results
				query.on('end', () => {
					done();
					return res.json(results);
				});
			}else{
				return res.status(200).json({success: false, data: "DUPLICATE"});
			}
		});
	});
});

module.exports = app;
