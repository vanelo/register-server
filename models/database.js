const pg = require("pg");

module.exports = {
	setup
};

async function setup(user, pass, host)
{
	const connectionString = "postgresql://" + user + ":" + pass + "@" + host + "/docsregister";
	const tables = [
		{
			name: "countries",
			columns: "id SERIAL PRIMARY KEY, name TEXT not null, countryIsoCode TEXT not null"
		},
		{
			name: "states",
			columns: "id SERIAL PRIMARY KEY, name TEXT not null, stateIsoCode TEXT not null, countryIsoCode TEXT not null, country_id TEXT not null"
		},
		{
			name: "doctypes",
			columns: "id SERIAL PRIMARY KEY, name TEXT not null, docTypecode TEXT not null"
		},
		{
			name: "documents",
			columns: "id SERIAL PRIMARY KEY, docTypecode TEXT not null, docNumber TEXT not null, countryIsoCode TEXT not null, stateIsoCode TEXT not null, docImageData1 TEXT not null, docImageData2 TEXT not null, optionalimage1 TEXT, optionalimage2 TEXT"
		}
	];

	for(let table of tables)
	{
		await createTable(table.name, table.columns, connectionString);
	}
}

//----------------------------
//functions
//----------------------------
async function createTable(tableName, columns, connectionString){
	var client = new pg.Client(connectionString);
	await client.connect();
	var query = await client.query("CREATE TABLE IF NOT EXISTS "+ tableName+"("+columns+")");
	await client.end();
}