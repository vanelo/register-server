# Modo de instalacion de la APP

## Instalar dependencias
- Nodejs
- Postgresql
- Supervisor: npm install supervisor -g
- Librerias: npm install

## Ejecutar el servidor
npm start

# Base de datos

## Para importar la base de datos 

pg_dump -U username databasename >>sqlfile.sql

Obs.: Verificar los permisos de acceso a la BD en pg_hba.conf
        # "local" is for Unix domain socket connections only
        host    all             all             0.0.0.0/0               trust